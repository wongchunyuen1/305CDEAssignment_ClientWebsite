var config = {
                    apiKey: "AIzaSyBjPs19_HsIW8EQ-d8Iqa_MFT-qfheIS5g",
                    authDomain: "wild-doctor.firebaseapp.com",
                    databaseURL: "https://wild-doctor.firebaseio.com",
                    projectId: "wild-doctor",
                    storageBucket: "wild-doctor.appspot.com",
                    messagingSenderId: "23522572045"
  };
firebase.initializeApp(config);
var database = firebase.database();

if (window.location.hash=='#home') {
    document.getElementById('Home').style.display = 'block';
	document.getElementById('Get').style.display='none';
	document.getElementById('Post').style.display='none';
	document.getElementById('Delete').style.display='none';
	document.getElementById('Order').style.display='none';
} else {
	document.getElementById('Get').style.display='none';
	document.getElementById('Post').style.display='none';
	document.getElementById('Delete').style.display='none';
	document.getElementById('Order').style.display='none';
}

document.getElementById('homeBtn').onclick=function() {
    document.getElementById('Delete').style.display = 'none';
    document.getElementById('Post').style.display = 'none';
    document.getElementById('Get').style.display = 'none';
    document.getElementById('Home').style.display = 'block';
    document.getElementById('Order').style.display='none';
}

document.getElementById('getBtn').onclick=function() {
    document.getElementById('Delete').style.display = 'none';
    document.getElementById('Post').style.display = 'none';
    document.getElementById('Get').style.display = 'block';
    document.getElementById('Home').style.display = 'none';
    document.getElementById('Order').style.display='none';
}

document.getElementById('postBtn').onclick=function() {
    document.getElementById('Delete').style.display = 'none';
    document.getElementById('Post').style.display = 'block';
    document.getElementById('Get').style.display = 'none';
    document.getElementById('Home').style.display = 'none';
    document.getElementById('Order').style.display='none';
}

document.getElementById('deleteBtn').onclick=function() {
    document.getElementById('Delete').style.display = 'block';
    document.getElementById('Post').style.display = 'none';
    document.getElementById('Get').style.display = 'none';
    document.getElementById('Home').style.display = 'none';
    document.getElementById('Order').style.display='none';
}

document.getElementById('orderBtn').onclick=function() {
    document.getElementById('Delete').style.display = 'none';
    document.getElementById('Post').style.display = 'none';
    document.getElementById('Get').style.display = 'none';
    document.getElementById('Home').style.display = 'none';
    document.getElementById('Order').style.display='block';
}

document.getElementById('ordersubmit').onclick=function() {
	var email = document.getElementById('email').value.toString();
	var order = document.getElementById('order').value.toString();
	if(email.length <= 0 || order.length <= 0){
	    if(email.length <= 0 && order.length <= 0){alert("Plz enter Email & Order");}
	    if(email.length <= 0 && order.length > 1){alert("Plz enter Email");}
	    if(email.length > 1 && order.length <= 1){alert("Plz enter Order");}
	} else {
        var checkEmail = /\S+@\S+\.\S+/;
        if(checkEmail.test(email)==false){
            alert("Plz check your Email");
        } else {
            firebase.database().ref('/Comment/' + Date()).set({
            email: email,
            order : order,
            time : Date()
            });
            alert("Thank You.");
        }
	}
};

document.getElementById('getsubmit').onclick=function() {
	var text = document.getElementById('getbt').value.toString();
	if(text.length <= 0){
	    alert("Plz enter plant's name");
	} else {
	    alert("It may take some time");
        var http = new XMLHttpRequest();
        $.ajax({
            url: "https://assignment2-wongchunyuen1.c9users.io?name="+text,
            type: "GET",
            dataType: "json",
            success: function(data){
                var json = $.parseJSON(data);
                console.log(json);
                console.log(json['State']);
                console.log(json['Message']);
                document.getElementById("gResultName").innerHTML=json['State'];
                document.getElementById("gResultDescription").innerHTML=json['Message'];

            },
            error: function(){
                console.log("error");
                document.getElementById("gResultName").innerHTML="Fair to find";
            }
        });
	}
};

document.getElementById('postsubmit').onclick=function() {
	var textn = document.getElementById('postnbt').value.toString();
	var textd = document.getElementById('postdbt').value.toString();
	if(textn.length <= 0 && textd.length <= 0){
	    alert("Plz enter plant's name or description");
	} else {
	    alert("It may take some time");
        var http = new XMLHttpRequest();
        $.ajax({
            url: "https://assignment2-wongchunyuen1.c9users.io",
            data: {name : textn,description : textd},
            type: "POST",
            dataType: "json",
            success: function(data){
                var json = $.parseJSON(data);
                console.log(json);
                console.log(json['State']);
                console.log(json['Message']);
                document.getElementById("pResultName").innerHTML=json['State'];
                document.getElementById("pResultDescription").innerHTML=json['Message'];
            },
            error: function(){
                console.log("error");
                document.getElementById("pResultName").innerHTML="Upload Fair";
            }
        });
	}
};

document.getElementById('deletesubmit').onclick=function() {
	var deletetext = document.getElementById('deletebt').value.toString();
	if(deletetext.length <= 0){
	    alert("Plz enter plant's name");
	} else {
	    alert("It may take some time");
        var http = new XMLHttpRequest();
        $.ajax({
            url: "https://assignment2-wongchunyuen1.c9users.io?name="+deletetext,
            type: "DEL",
            dataType: "json",
            success: function(data){
                var json = $.parseJSON(data);
                console.log(json);
                console.log(json['State']);
                console.log(json['Message']);
                document.getElementById("dResultName").innerHTML=json['State'];
                document.getElementById("dResultDescription").innerHTML=json['Message'];

            },
            error: function(){
       var ref = database.ref("/Test/" + deletetext);
       ref.once("value", function(snapshot) {
       if (snapshot.val() != null) {
           database.ref("/Test/" + deletetext).remove()
           document.getElementById("dResultName").innerHTML= deletetext + " is deleted.";
       } else {
           document.getElementById("dResultName").innerHTML= deletetext + " can't delete because it is not on database.";
        }
       });
            }
        });
	}
};